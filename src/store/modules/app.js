import Cookies from 'js-cookie'

const app = {
  state: {
    // 导航按钮
    sidebar: {
      opened: !+Cookies.get('sidebarStatus')
    },
    device: 'desktop',
    basecolor: 'heise'// 配色方案
  },
  mutations: {
    TOGGLE_SIDEBAR: state => {
      if (state.sidebar.opened) {
        Cookies.set('sidebarStatus', 1)
      } else {
        Cookies.set('sidebarStatus', 0)
      }
      state.sidebar.opened = !state.sidebar.opened
    },
    CLOSE_SIDEBAR: (state) => {
      Cookies.set('sidebarStatus', 1)
      state.sidebar.opened = false
    },
    TOGGLE_BASECOLOR: (state, data) => {
      state.sidebar.basecolor = data
    }
  }
}
export default app
