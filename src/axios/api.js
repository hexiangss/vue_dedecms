import { fetch } from './fetch' // 引用fetch.js
import api from './url' // 引用url.js
// 查看用户
export function lookOption (issuer, userId) {
  // lookOption是你要调用接口的名字，issuer,userId是传进来的参数
  return fetch({
    // api.Hallowmas 引用url.js里面的数据
    url: api.Hallowmas + '/halloween/' + issuer + '/question',
    method: 'get', // 请求方法
    params: {
      currentUserId: userId // 传过去的参数
    }
  })
}
// 登录
export function login (data) {
  return fetch({
    url: api.login + '/login.php',
    method: 'post', // 请求方法
    params: data
  })
}
// 登出
export function exit (data) {
  return fetch({
    url: api.login + '/exit.php',
    method: 'post'
  })
}
// 后台列表
export function contentlists () {
  return fetch({
    url: api.login + '/menu_ajax.php',
    method: 'get' // 请求方法
  })
}
// 后台文档列表
export function contentlistarc (typeid, channelid, page, pagesize, orderby, orderWay) {
  return fetch({
    url: api.login + '/content_list_ajax.php',
    method: 'get', // 请求方法
    params: {
      cid: typeid, // 传过去的参数
      channelid: channelid || 1,
      page: page || 1,
      pagesize: pagesize || 20,
      orderby: orderby || '',
      orderWay: orderWay || ''
    }
  })
}
// 文章详情修改
export function arcajax (id) {
  return fetch({
    url: api.login + '/article_edit_ajax.php',
    method: 'get', // 请求方法
    params: {
      aid: id // 传过去的参数'
    }
  })
}
// 文章详情添加获取参数
export function arcaddajax (channelid) {
  return fetch({
    url: api.login + '/article_add_ajax.php',
    method: 'get', // 请求方法
    params: {
      channelid: channelid // 传过去的参数'
    }
  })
}
// 文章详情发布
export function arcaddajaxData (data) {
  return fetch({
    url: api.login + '/article_add_ajax.php',
    method: 'get', // 请求方法
    params: data
  })
}

export function lists (typeid, page, pagesize, orderby, orderWay) {
  return fetch({
    url: api.plus + '/plus/ajax/ajax_list.php',
    method: 'get', // 请求方法
    params: {
      ajax: 'sjfk',
      typeid: typeid, // 传过去的参数
      page: page || 1,
      pagesize: pagesize || 10,
      orderby: orderby || '',
      orderWay: orderWay || ''
    }
  })
}

export function arclist (typeid, page, pagesize, fields) {
  var params = {
    ajax: 'sjfk',
    typeid: typeid, // 传过去的参数
    page: page || 1,
    pagesize: pagesize || 10
  }
  if (fields !== '') {
    params = Object.assign(params, fields)
  }
  return fetch({
    url: api.plus + '/plus/ajax/ajax_arclist.php',
    method: 'get', // 请求方法
    params: params
  })
}
export function article (aid, channel) {
  return fetch({
    url: api.plus + '/plus/ajax/view_ajax.php',
    method: 'get', // 请求方法
    params: {
      ajax: 'sjfk',
      aid: aid, // 传过去的参数
      channel: channel || 1
    }
  })
}
export function typemenu (typeid, type, field) {
  return fetch({
    url: api.plus + '/plus/ajax/type_ajax.php',
    method: 'get', // 请求方法
    params: {
      type: type || 'ajax',
      typeid: typeid || 0,
      field: field || 'typename,id,icon,channeltype'
    }
  })
}
export function channeltype (channel, type) {
  return fetch({
    url: api.plus + '/plus/ajax/ajax_channeltype.php',
    method: 'get', // 请求方法
    params: {
      type: type || 'ajax',
      channel: channel
    }
  })
}
// 有新接口的时候像上面那样再来一次
// // 修改昵称接口
// export function userID(name){
//   return fetch({
//     url:api.myself_name,
//     method:"put",
//     data:{
//       nickname:name
//     }
//   })
// }
//
//
// //取消转发赞踩接口
// export function cancelForward(articleId,type){
//   return fetch({
//     url:api.detail_article+articleId+"/forwarded_impress",
//     method:"delete",
//     params:{
//       type:type
//     }
//   })
// }
