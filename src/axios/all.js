// flag 的头条属性
export function flag (value) {
  if (value) {
    return value.indexOf('h') !== -1
  } else {
    return false
  }
}
// 计算模板的方法
export function tem (value) {
  return value === '30' ? 'liststudy' : value === '9' ? 'listlable' : 'list'
}
