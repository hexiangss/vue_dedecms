import axios from 'axios' // 引入axios
import store from './../store/index'
export function fetch (options) {
  return new Promise((resolve, reject) => {
    const instance = axios.create({
      // instance创建一个axios实例，可以自定义配置，可在 axios文档中查看详情
      // 所有的请求都会带上这些配置，比如全局都要用的身份信息等。
      headers: {
        'Content-Type': 'application/json',
        'token_header': store.getters.token // token从全局变量那里传过来
      },
      async: false,
      timeout: 30 * 1000 // 30秒超时
    })
    instance.interceptors.request.use(
      function (config) {
        // 在发送请求之前做些什么
        return config
      },
      function (error) {
        // 对请求错误做些什么
        return Promise.reject(error)
      }
    )
    // 响应拦截器
    instance.interceptors.response.use(
      function (config) {
        // 对响应数据做点什么
        if (config.data.state === '-1') {
          location.href = '/#/login'
        } else {
          return config
        }
      },
      function (error) {
        // 对响应错误做点什么
        return Promise.reject(error)
      }
    )
    instance(options)
      .then(response => {
        // then 请求成功之后进行什么操作
        resolve(response)
        // 把请求到的数据发到引用请求的地方
      })
      .catch(error => {
        console.log('请求异常信息：' + error)
        reject(error)
      })
  })
}
