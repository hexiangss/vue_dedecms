// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router.js'
import store from './store/index'
import ElementUI from 'element-ui'
import './assets/less/all.less'
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/theme-chalk/display.css'
import axios from 'axios'
/* element的msg封装 */
import msg from './common/message/message'
/* eslint-disable no-new */
/* eslint-disable no-new */
Vue.prototype.$axios = axios
Vue.prototype.$msg = msg
Vue.config.productionTip = false

Vue.use(ElementUI)
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
