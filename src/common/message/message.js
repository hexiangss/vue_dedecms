export function msgs (msg, type, times, Close) {
  let types = ''
  let duration = times || 3000
  let showClose = Close || 1
  switch (type) {
    case 1:
      types = 'success'
      break
    case 2:
      types = 'warning'
      break
    case 3:
      types = 'error'
      duration = 0
      showClose = 0
      break
    default:
      types = ''
  }
  this.$message({
    type: types,
    message: msg,
    duration: duration,
    showClose: !showClose
  })
}

export default msgs
